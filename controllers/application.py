def index():
    apps = db().select(db.application.ALL)
    return dict(apps = apps)

def insert():
    record = None
    if len(request.args) == 1:
        id = request.args[0]
        query = db(db.application.id == id)
        if query.count() == 1:
            record = query.select()[0]
    
    form = SQLFORM(db.application, show_id = False, record = record, formstyle = 'divs', submit_button = 'Save')

    if form.accepts(request.vars, formname = 'default'):
        response.flash = 'Application saved successfully'
        redirect(URL('index'))
    elif form.errors:
        response.flash = 'Please, fill the form correctly'
    
    return dict(form = form)

def remove():
    if len(request.args) == 1:
        id = request.args[0]
        db(db.application.id == id).delete()
    
    session.flash = 'Application removed successfully'
    redirect(URL('index'))
    return 

def getLocations():
    locations = db(db.location.id_application == request.args[0]).select()
    return response.json(locations.as_list())
    
def defineLocations():
    vars = request.vars
    record = None
    if len(request.args) == 1:
        id = request.args[0]            
        query = db(db.application.id == id)
        if query.count() == 1:
            record = query.select()[0]
    
    if vars.has_key('locationName[]'):
        geoNameArray = vars['locationName[]']
        geoLatArray = vars['locationLatitude[]']
        geoLongArray = vars['locationLongitude[]']
        geoRadiusArray = vars['locationRadius[]']
        
        if not isinstance(geoNameArray, (list, tuple)):
            geoNameArray = [geoNameArray]
            geoLatArray = [geoLatArray]
            geoLongArray = [geoLongArray]
            geoRadiusArray = [geoRadiusArray]
            
        for i in range(len(geoNameArray)):
            db.location.insert(id_application = record.id, 
                               name = geoNameArray[i],
                               lat = float(geoLatArray[i]),
                               lon = float(geoLongArray[i]),
                               radius = int(geoRadiusArray[i])) 
    
    if vars.has_key('form'):
        session.flash = 'Locations saved successfully'
        redirect(URL('index'))            
    
    return dict(app = record)
