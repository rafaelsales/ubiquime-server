import sys
sys.path.append('/home/rafaelcds/rafael.pyboys.com/w2p/applications/ubiquime/modules/lib.linux-x86_64-2.5')

from urllib2 import urlopen
from lxml import html

def getApplications():
    vars = request.vars
    lat = float(vars.lat)
    lon = float(vars.lon)
    
    q = 'SELECT DISTINCT(a.id), a.name, a.identifier, l.name AS locationName, l.lat, l.lon, ' \
        '       ROUND(SQRT(POW(l.lat - %(userLat)f, 2) + POW(l.lon - %(userLon)f, 2)) * 111000) AS distance ' \
        'FROM location l, application a ' \
        'WHERE l.id_application = a.id ' \
        '  AND SQRT(POW(l.lat - %(userLat)f, 2) + POW(l.lon - %(userLon)f, 2)) * 111000 <= l.radius ' % \
        {'userLat': lat, 'userLon': lon}
     
    rows = db.executesql(q, as_dict = True)
    for row in rows:
        row['icon'] = getApplicationIcon(row['identifier'])
    
    return response.json(rows)

def getApplicationIcon(appPackageIdentifier):
    url = 'https://play.google.com/store/search?q=%s&c=apps' % appPackageIdentifier
    page = urlopen(url)
    page = html.parse(page).getroot()
    icons = page.cssselect('.thumbnail img')
    
    if len(icons) == 1:
        return icons[0].get('src')
    return None
