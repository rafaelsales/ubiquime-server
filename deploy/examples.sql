INSERT INTO `application` (`id`,`identifier`,`name`) VALUES (1,'com.iguatemi.android','Iguatemi App');
INSERT INTO `application` (`id`,`identifier`,`name`) VALUES (2,'br.ufc.pici.npd','UFC NPD App');
INSERT INTO `application` (`id`,`identifier`,`name`) VALUES (3,'br.ufc.pici.great','UFC GREat App');
INSERT INTO `application` (`id`,`identifier`,`name`) VALUES (4,'br.ufc.pici.dc','UFC Departamento de Computação App');
INSERT INTO `application` (`id`,`identifier`,`name`) VALUES (5,'br.ufc.pici.biblioteca','UFC Bibliotecas App');

INSERT INTO `location` (`id`,`id_application`,`name`,`radius`,`lon`,`lat`) VALUES (1,1,'Iguatemi App',400,-38.490629,-3.751037);
INSERT INTO `location` (`id`,`id_application`,`name`,`radius`,`lon`,`lat`) VALUES (2,2,'NPD',200,-38.575032,-3.745523);
INSERT INTO `location` (`id`,`id_application`,`name`,`radius`,`lon`,`lat`) VALUES (3,3,'Pici - Great',200,-38.577977,-3.746502);
INSERT INTO `location` (`id`,`id_application`,`name`,`radius`,`lon`,`lat`) VALUES (4,4,'Pici - DC',200,38.574211,-3.746056);
INSERT INTO `location` (`id`,`id_application`,`name`,`radius`,`lon`,`lat`) VALUES (5,5,'Biblioteca da Matemática',200,-38.573932,-3.746396);
INSERT INTO `location` (`id`,`id_application`,`name`,`radius`,`lon`,`lat`) VALUES (6,5,'Biblioteca de Ciências e Tecnologia',400,-38.574358,-3.742496);
INSERT INTO `location` (`id`,`id_application`,`name`,`radius`,`lon`,`lat`) VALUES (7,5,'Biblioteca da Física',200,-38.575351,-3.746982);
